#include <iostream>
#include <stdlib.h>
using namespace std;

struct nodo{
	char producto;
	nodo *sgte;
};

typedef nodo * cola;


void menu(cola &Tcola);
 void limpiarBuffer();
void push_cola(cola &Tcola);
void eliminar_porvalor(cola &Tcola);

nodo* obtener_nodo(cola &Tcola);
void pop_cola(cola &Tcola);
 
 
void mostrar(cola &Tcola);
int main(){
	cola Tcola=NULL;

	menu(Tcola);
	
}

 void menu(cola &Tcola){
	char opcion;
	while(opcion!='4'){
		system("cls");
		cout<<endl<<endl<<"\t"<<char(201);
		for(int i=0; i<47; i++)	cout<<char(205);
		cout<<char(187)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t               MENU     \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t1. Ingresar datos.      \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t2. Eliminar Producto.   \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t3. Mostrar.             \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t4. Salir                \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(200);
		for(int i=0; i<47; i++)	cout<<char(205);
		cout<<char(188)<<endl;
		
		cout<<"\tIngrese opcion: ";
		cin>>opcion;
		cout<<endl<<endl;
	 	switch (opcion){
		 	case '1': push_cola(Tcola); break;
		 	case '2': eliminar_porvalor(Tcola); break;
		 	case '3': mostrar(Tcola); break;
		 	case '4': system("exit");  break;
		 	default: cout << "\n\t\t\t\tOpcion invalida!";
	 	}
		limpiarBuffer();
	}
}
 void limpiarBuffer(){
 	system("pause > nul");
 	std::cin.clear();
 	std::cin.ignore();
 
 }
void push_cola(cola &Tcola){
 	cola nuevo = new (nodo);
 	cola aux=Tcola;
 	if (nuevo == NULL){
 		cout << "Error de memoria!" << endl;
 	}
 	else{
 	
 		cout << "Digite nuevo elemento (solo letras): ";
 		cin >> nuevo->producto;
 		nuevo->sgte = NULL;
 		if (Tcola == NULL){
 			Tcola = nuevo;
 		}
 		else{
 			while(aux->sgte!=NULL){
 				aux=aux->sgte;
 			}
 			aux->sgte=nuevo;
 		}
 	}
 }
 
void push_cola(cola &Tcola,nodo* nuevo){
	cola aux = Tcola;
 	if (Tcola == NULL){
 		Tcola = nuevo;
 	}
 	else{
 		while(aux->sgte!=NULL){
 			aux=aux->sgte;
 		}
 		aux->sgte=nuevo;
 	}	
}
void eliminar_porvalor(cola &Tcola){
	char valor;
	if(Tcola == NULL ){
		cout<<"Cola vacia"<<endl;
	}else{
		cout<<"Que producto de la cola desea eliminar: "; cin>>valor;
		char inicio = Tcola->producto;
		bool eliminado = false;
		cola aux = NULL;
		do{
			if(Tcola->producto == valor){
				if(Tcola->producto == inicio){
					pop_cola(Tcola);
					break;
				}else{
					pop_cola(Tcola);
				}
				eliminado=true;
			}
			else{
				aux=obtener_nodo(Tcola);
				push_cola(Tcola,aux);
			}
		}while(Tcola->producto != inicio);
		if(eliminado){
			cout<<"Producto eliminado!"<<endl;
		}else{
			cout<<"Producto no encontrado!"<<endl;
		}
	}
}

nodo* obtener_nodo(cola &Tcola){
	if(Tcola == NULL){
		cout<<"Cola vacia"<<endl;
		return NULL;
	}else{
		cola aux=Tcola;
		Tcola = Tcola->sgte;
		aux->sgte=NULL;
		return aux;		
	}
}

void pop_cola(cola &Tcola){
	if(Tcola == NULL ){
		cout<<"Cola vacia"<<endl;
	}else{
		cola aux=Tcola;
		Tcola = Tcola->sgte;
		delete aux;		
	}	
}
 
 
void mostrar(cola &Tcola){
 	if (Tcola == NULL){
 		cout << "Lista vacia!" << endl;
 	}
 	else{
 		cola aux = Tcola;
 		while (aux != NULL){
 			cout <<aux->producto <<" ";
 			aux = aux->sgte;
 		}
 	}
 }

