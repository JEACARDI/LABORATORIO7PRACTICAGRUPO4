#include <iostream>
#include <stdlib.h>

using namespace std;

class lista{
	int *miLista;
	int cantidad;
	int *final;
	void unomas(int aux[]){
		miLista = new int[cantidad+1];
		for(int i=0; i<cantidad; i++){
			miLista[i]=aux[i];
		}
		cantidad++;
		delete[] aux;
	}
	void cambio(int& num1, int& num2){
    	int temp = num1;
    	num1 = num2;
    	num2 = temp;
	}

	void quicksort(int miLista[], int min, int max){
	    int pivote = miLista[min];
	    int izq = min;
		int der = max;
	    while (izq < der) {
	        while (miLista[izq] < pivote) {
	            izq++;
	        }
	        while (miLista[der] > pivote) {
	            der--;
	        }
	
	        if (izq <= der) {
	            cambio(miLista[izq], miLista[der]);
	            cout<<"Pivote actual: "<<pivote<<endl;
	            mostrar();
	            izq++;
	            der--;
	        }
	    }
	    if (min < der) {
	        quicksort(miLista, min, der);
	    }
	    if (izq < max) {
	        quicksort(miLista, izq, max);
	    }
	}
	
	public:
		lista(){
			miLista = new int[0];
			cantidad = 0;
			final = NULL;
		}
		void insertar_final(int nuevo){
			unomas(miLista);
			miLista[cantidad-1] = nuevo;
			final=&miLista[cantidad-1];
		}
		
		void mostrar(){
			if(cantidad==0)
				cout<<"Lista vacia!"<<endl;
			else{
				cout<<"Lista actual: ";
				for(int i=0; i<cantidad; i++)
					cout<<miLista[i]<<" ";
				cout<<endl;
			}
		}
		
	void ordenamiento_quicksort(){
		quicksort(miLista,0,cantidad);
		cout<<"Ordenado!"<<endl;
	}
};

void menu();

typedef lista* TLista;

int main(){
	menu();
}

void menu(){
	char opc;
	TLista miArreglo = new lista();
	while(opc!='4'){
		system("cls");
		cout<<endl<<endl<<"\t"<<char(201);
		for(int i=0; i<47; i++)	cout<<char(205);
		cout<<char(187)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t               MENU     \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t1. Insertar al final.   \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t2. Ordenar.             \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t3. Mostrar.             \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t4. Salir.\t\t\t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(186)<<"\t                        \t\t"<<char(186)<<endl;
		cout<<"\t"<<char(200);
		for(int i=0; i<47; i++)	cout<<char(205);
		cout<<char(188)<<endl;
		
		cout<<"\tIngrese opcion: ";
		cin>>opc;
		cout<<endl<<endl;
		switch(opc){
			case '1': cout<<"Ingrese valor a insertar: "; int val; cin>>val;
					  miArreglo->insertar_final(val);
			break; 
			case '2': miArreglo->ordenamiento_quicksort();
			break;
			case '3': miArreglo->mostrar();
			break;
			case '4': 
			break;
			default:	cout<<"Opcion invalida!"<<endl;
		}
		system("pause > null");
	}
}
