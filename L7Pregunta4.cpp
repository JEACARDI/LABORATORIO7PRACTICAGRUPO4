#include<iostream>
using namespace std;

struct nodo{
	char identificador;
	nodo* ant;
	nodo* sgte;
};

typedef nodo* Tlista;

void agregar_despuesde(Tlista &miLista,char identificador,char identificadorAnterior);

void mostrar_lista_i(Tlista miLista);

void incorporar_nodo(Tlista &miLista);
void incorporar_nodo_inicio(Tlista &miLista);
nodo* obtener_nodo(Tlista &miLista, char identificador);

void menu(Tlista &miLista);
	
void crear_nodos(Tlista &miLista);

void insertar_inicio(Tlista &miLista, char indentificador);

void insertar_inicio_nodo(Tlista &miLista, nodo* nuevo);

void agregar_despuesde_nodo(Tlista &miLista,nodo* nuevo,char identificadorAnterior);
void convertir(Tlista &miLista);

int main(){
	Tlista miLista=NULL;
	menu(miLista);
}

void menu(Tlista &miLista){
	int opc;
	while(opc!=6){
		cout<<"1. Crear lista."<<endl;
		cout<<"2. Convertir."<<endl;
		cout<<"3. Mostrar."<<endl;
		cin>>opc;
		switch(opc){
			case 1: crear_nodos(miLista);
			break;
			case 2:	convertir(miLista);
			break;
			case 3: mostrar_lista_i(miLista);
			break;
		}
	}
}

void incorporar_nodo(Tlista &miLista){
	char nuevoNodo,antiguoNodo;
	cout<<"Ingrese identificador del nodo a incorporar: ";
	cin>>nuevoNodo;
	cout<<"Incorporar despues de: ";
	cin>>antiguoNodo;
	Tlista aux = miLista;
	while(aux->identificador!=antiguoNodo){
		aux=aux->sgte;
	}
	agregar_despuesde(miLista,nuevoNodo,antiguoNodo);
	cout<<"Nuevo nodo agregada!"<<endl;
}

void incorporar_nodo_inicio(Tlista &miLista){
	char nuevoNodo;
	cout<<"Ingrese identificador del nodo a incorporar: ";
	cin>>nuevoNodo;
	Tlista aux = miLista;
	insertar_inicio(miLista,nuevoNodo);
	cout<<"Nuevo nodo agregada!"<<endl;
}

void agregar_despuesde(Tlista &miLista,char identificador,char identificadorAnterior){
	Tlista nuevo = new (nodo);
	nuevo->identificador=identificador;
	nuevo->ant=NULL;	nuevo->sgte=NULL;
	if(miLista==NULL){
		miLista = nuevo;
	}else{
		Tlista aux = miLista;
		while(aux->identificador!=identificadorAnterior){
			aux = aux->sgte;
		}
		nuevo->ant = aux;
		if(aux->sgte!=NULL){
			nuevo->sgte = aux->sgte;
			aux->sgte->ant = nuevo;
			aux->sgte = nuevo;
		}else{
			aux->sgte=nuevo;
		}
	}
}

void mostrar_lista_i(Tlista miLista){
	if(miLista==NULL){
		cout<<"No hay nodos!"<<endl;
	}else{
		Tlista aux = miLista;
		while(aux!=NULL){
			cout<<aux->identificador<<" ";
			aux=aux->sgte;
		}
		cout<<endl;		
	}
}

void mostrar_lista_d(Tlista miLista){
	if(miLista==NULL){
		cout<<"No hay nodos!"<<endl;
	}else{
		Tlista aux = miLista;
		while(aux->sgte!=NULL){
			aux = aux->sgte;
		}
		while(aux!=NULL){
			cout<<aux->identificador<<" ";
			aux=aux->ant;
		}
		cout<<endl;		
	}
}
void crear_nodos(Tlista &miLista){
	insertar_inicio(miLista,'N');
	insertar_inicio(miLista,'O');
	insertar_inicio(miLista,'M');
	insertar_inicio(miLista,'A');
	insertar_inicio(miLista,'R');
	cout<<"Lista creada!"<<endl;
	mostrar_lista_i(miLista);
}

void convertir(Tlista &miLista){
	Tlista N = obtener_nodo(miLista,'N');
	insertar_inicio_nodo(miLista,N);
	mostrar_lista_i(miLista);
	Tlista O = obtener_nodo(miLista,'O');
	agregar_despuesde_nodo(miLista,O,'N');
	mostrar_lista_i(miLista);
	Tlista M = obtener_nodo(miLista,'M');
	agregar_despuesde_nodo(miLista,M,'R');
	mostrar_lista_i(miLista);
	cout<<"RAMON A NORMA!"<<endl;
}

nodo* obtener_nodo(Tlista &miLista, char identificador){
	Tlista aux = miLista;
	bool encontrado=false;
	while(aux != NULL){
		aux=aux->sgte;
		if(aux->identificador == identificador){
			encontrado=true;
			break;	
		}
	}
	if(encontrado){
		if(aux->ant == NULL){
			miLista = aux->sgte;
			miLista->ant = NULL;
		}else if(aux->sgte == NULL){
			aux->ant->sgte = NULL;
		}else{
			aux->sgte->ant = aux->ant;
			aux->ant->sgte = aux->sgte;
		}
		return aux;
	}else{
		return NULL;
	}
}

void insertar_inicio(Tlista &miLista, char identificador){
	Tlista nuevo = new (nodo);
	nuevo->ant = NULL;	nuevo->sgte = NULL; nuevo->identificador = identificador;
	if(miLista==NULL){
		miLista = nuevo;
	}else{
		nuevo->sgte = miLista;
		miLista->ant = nuevo;
		miLista = nuevo;
	}
}

void insertar_inicio_nodo(Tlista &miLista, nodo* nuevo){
	nuevo->ant = NULL;	nuevo->sgte = NULL;
	if(miLista==NULL){
		miLista = nuevo;
	}else{
		nuevo->sgte = miLista;
		miLista->ant = nuevo;
		miLista = nuevo;
	}
}

void agregar_despuesde_nodo(Tlista &miLista,nodo* nuevo,char identificadorAnterior){
	nuevo->ant=NULL;	nuevo->sgte=NULL;
	if(miLista==NULL){
		miLista = nuevo;
	}else{
		Tlista aux = miLista;
		while(aux->identificador!=identificadorAnterior){
			aux = aux->sgte;
		}
		nuevo->ant = aux;
		if(aux->sgte!=NULL){
			nuevo->sgte = aux->sgte;
			aux->sgte->ant = nuevo;
			aux->sgte = nuevo;
		}else{
			aux->sgte=nuevo;
		}
	}
}
