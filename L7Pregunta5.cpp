#include<iostream>
using namespace std;

struct nodo{
	char identificador;
	nodo* ant;
	nodo* sgte;
};

typedef nodo* Tlista;

void agregar_despuesde(Tlista &miLista,char identificador,char identificadorAnterior);

void mostrar_lista_d(Tlista miLista);
void mostrar_lista_i(Tlista miLista);

void incorporar_nodo(Tlista &miLista);
void incorporar_nodo_inicio(Tlista &miLista);

void menu(Tlista &miLista);
	
void crear_nodos(Tlista &miLista);

void insertar_inicio(Tlista &miLista, char indentificador);

int main(){
	Tlista miLista=NULL;
	menu(miLista);
}

void menu(Tlista &miLista){
	int opc;
	while(opc!=6){
		cout<<"1. Crear lista."<<endl;
		cout<<"2. Insertar inicio."<<endl;
		cout<<"3. Insertar despues de."<<endl;
		cout<<"4. Recorrer de izquierda a derecha."<<endl;
		cout<<"5. Recorrer de derecha a izquierda."<<endl;
		cin>>opc;
		switch(opc){
			case 1: if(miLista == NULL ) crear_nodos(miLista);
					else	cout<<"Ya creo la lista!"<<endl;
			break;
			case 2:	incorporar_nodo_inicio(miLista);
			break;
			case 3: incorporar_nodo(miLista);
			break;
			case 4: mostrar_lista_i(miLista);
			break;
			case 5: mostrar_lista_d(miLista);
			break;
		}
	}
}

void incorporar_nodo(Tlista &miLista){
	char nuevoNodo,antiguoNodo;
	cout<<"Ingrese identificador del nodo a incorporar: ";
	cin>>nuevoNodo;
	cout<<"Incorporar despues de: ";
	cin>>antiguoNodo;
	Tlista aux = miLista;
	while(aux->identificador!=antiguoNodo){
		aux=aux->sgte;
	}
	agregar_despuesde(miLista,nuevoNodo,antiguoNodo);
	cout<<"Nuevo nodo agregada!"<<endl;
}

void incorporar_nodo_inicio(Tlista &miLista){
	char nuevoNodo;
	cout<<"Ingrese identificador del nodo a incorporar: ";
	cin>>nuevoNodo;
	Tlista aux = miLista;
	insertar_inicio(miLista,nuevoNodo);
	cout<<"Nuevo nodo agregada!"<<endl;
}

void agregar_despuesde(Tlista &miLista,char identificador,char identificadorAnterior){
	Tlista nuevo = new (nodo);
	nuevo->identificador=identificador;
	nuevo->ant=NULL;	nuevo->sgte=NULL;
	if(miLista==NULL){
		miLista = nuevo;
	}else{
		Tlista aux = miLista;
		while(aux->identificador!=identificadorAnterior){
			aux = aux->sgte;
		}
		nuevo->ant = aux;
		if(aux->sgte!=NULL){
			nuevo->sgte = aux->sgte;
			aux->sgte->ant = nuevo;
			aux->sgte = nuevo;
		}else{
			aux->sgte=nuevo;
		}
	}
}

void mostrar_lista_i(Tlista miLista){
	if(miLista==NULL){
		cout<<"No hay nodos!"<<endl;
	}else{
		Tlista aux = miLista;
		while(aux!=NULL){
			cout<<aux->identificador<<" ";
			aux=aux->sgte;
		}
		cout<<endl;		
	}
}

void mostrar_lista_d(Tlista miLista){
	if(miLista==NULL){
		cout<<"No hay nodos!"<<endl;
	}else{
		Tlista aux = miLista;
		while(aux->sgte!=NULL){
			aux = aux->sgte;
		}
		while(aux!=NULL){
			cout<<aux->identificador<<" ";
			aux=aux->ant;
		}
		cout<<endl;		
	}
}
void crear_nodos(Tlista &miLista){
	insertar_inicio(miLista,'L');
	insertar_inicio(miLista,'M');
	insertar_inicio(miLista,'N');
	cout<<"Lista creada!"<<endl;
	mostrar_lista_i(miLista);
}
void insertar_inicio(Tlista &miLista, char identificador){
	Tlista nuevo = new (nodo);
	nuevo->ant = NULL;	nuevo->sgte = NULL; nuevo->identificador = identificador;
	if(miLista==NULL){
		miLista = nuevo;
	}else{
		nuevo->sgte = miLista;
		miLista->ant = nuevo;
		miLista = nuevo;
	}
}
